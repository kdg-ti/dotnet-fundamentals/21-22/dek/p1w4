﻿namespace CA
{
    public enum Gender : byte
    {
        M = 1,
        F = 5,
        X = 10
    }
}