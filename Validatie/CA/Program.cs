﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CA
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person()
            {
                Name = "Bill Gates",
                Gender = (Gender)8,
                Age = 85
            };

            var result = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(p, new ValidationContext(p), 
                result, validateAllProperties:true);

            Console.WriteLine("Is 'p' valide? -> "+valid);


            //Console.ReadLine();
        }
    }
}