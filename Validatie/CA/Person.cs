﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CA
{
    public class Person : IValidatableObject
    {
        /*private string _name;
        public string Name
        {
            get { return _name;}
            set
            {
                if (!(value.Length >= 3))
                    throw new Exception("Name has to be at least 3 characters");
                
                _name = value;
            }
        }*/
        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string Name { get; set; }
        
        public Gender Gender { get; set; }
        
        [Range(16, Int32.MaxValue)]
        public int Age { set; get; }
        

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            
            // Gender
            if (!Enum.IsDefined(typeof(Gender), Gender))
                results.Add(new ValidationResult("Gender moet M, V of X zijn",
                    new string[] {nameof(Gender)}));
                
            // ...

            return results;
        }
    }
}